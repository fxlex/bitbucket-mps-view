var params = {
    TYPE: 0,
    MODULE: 1,
    MODEL: 2,
    NODE: 3
};

var NodeInfo = function() {
    this.nodePath = undefined;
    this.type = undefined;
    this.module =  undefined;
    this.model = undefined;
    this.node = undefined;
    this.nodeID = undefined;
    this.conceptNode = undefined;
};

NodeInfo.prototype.init = function(nodePath) {
    var splitted = nodePath.split("@");
    this.nodePath = nodePath;
    this.type = splitted[0];
    this.module =  splitted[1];
    this.model = splitted[2];
    this.node = splitted[3];
    this.nodeID = splitted[4];
    this.conceptNode = splitted[5];
    this.conceptNode = this.conceptNode.substring(0,this.conceptNode.lastIndexOf("."));
    return this;
};

NodeInfo.prototype.equals = function(other) {
    return (this.module === other.module || other.module == undefined) &&
        (this.model === other.model || other.model == undefined) &&
        (this.node === other.node || other.node == undefined) &&
        (this.nodeID === other.nodeID || other.nodeID == undefined) &&
        (this.conceptNode === other.conceptNode || other.conceptNode == undefined);
};

NodeInfo.prototype.linkForType = function() {
    var type = this.type;
    if(this.type === "language") {
        type = "languages";
    } else if(this.type === "solution") {
        type = "solutions";
    }
    return "#" + type;
};

NodeInfo.prototype.linkForModule = function() {
    return "#type/" + this.type + "/module/" + this.module;
};

NodeInfo.prototype.linkForModel = function() {
    return "#type/" + this.type + "/module/" + this.module + "/model/" + this.model;
};

NodeInfo.prototype.linkForNode = function() {
    return "#type/" + this.type + "/module/" + this.module + "/model/" + this.model + "/nodeID/" + this.nodeID + "/nodeName/" + this.node;
};


NodeInfo.prototype.navigationFor = function(name) {
    addNavigationLevel(this[name],this['linkFor' + capitalizeFirstLetter(name)]())
};

NodeInfo.prototype.buildNavigation = function(param) {
    if(param >= params.TYPE)
        this.navigationFor("type");

    if(param >= params.MODULE)
        this.navigationFor("module");

    if(param >= params.MODEL)
        this.navigationFor("model");

    if(param >= params.NODE)
        this.navigationFor("node");
};