function readFileContent(filePath,success,error) {

    AP.require('request', function(request){
        request({
            url: '/1.0/repositories/' + repoPath + '/src/HEAD/' + filePath,
            success: function(response){
                success(response.data);
            },
            error: function(e) {
                error(e);
            }
        });
    });
}

function addItem(content,conceptNode,url,iconUrl,additional) {
    if(additional === undefined)
        additional = "";

    var listItem = $.templates(
        '<tr class="iterable-item">'+
            '<td class="name dirname\" colspan="4">'+
                '<img src="{{:icon}}" height="16px" class="icon1 align-middle">' +
                '<a href="{{:url}}" class="pjax-trigger execute align-middle conceptNodeToolTip" title="{{:title}}">'+
                '{{:content}}</a>'+
                '{{:additional}}' +
            '</td>'+
        '</tr>');

    var renderedItem = listItem.render(
        { icon: iconUrl, url: url, title: conceptNode, content: content, additional: additional});
    $('#mps-table').append($(renderedItem));
}

function clearListAndSVG() {
    $('#mps-table').empty();
    $('#svg').html("");
}

function addNavigationLevel(text,url) {
    var navList = $('#mps-navigation-list');
    var navItem = $.templates('<li><a href="{{:url}}">{{:text}}</a></li>');
    navList.append($(navItem.render({url: url, text: text})));
}

function resetNavigationLevel(name) {
    if(name === undefined)
        name = "Project";
    $('#mps-navigation-list').empty();
    addNavigationLevel(name,"#");
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function showError(title,message) {
    AJS.flag({
        type: 'error',
        title: title,
        body: message
    });
}

function showDefaultError(e) {
    showError(e.title, e.message);
}

function getModuleKind(path) {
    if(path.endsWith(".msd")) {
        return "solution";
    } else if(path.endsWith(".mpl")) {
        return "language";
    }
    return "unknown";
}

function toBase64Image(data) {
    return "data:image/png;base64," + data;
}

function localPath(path) {
    return "local:" + path;
}

function isLocalPath(path) {
    return path.startsWith("local:");
}

function getIcon(path,success,error) {
    var isLocal = isLocalPath(path);
    if(isLocal) {
        path = path.substr(6); // remove local: part
    }
    var cachedItem = $.jStorage.get(path, null);
    if(cachedItem !== null) {
        success(cachedItem);
    } else {
        var fullPath = "icons/" + path;
        if(isLocal) {
            toDataUrl(fullPath,function(base64Data) {
                $.jStorage.set(path, base64Data);
                success(base64Data);
            });
        } else {
            readFileContent(fullPath,function(data) {
                var base64Data = toBase64Image(data);
                $.jStorage.set(path, base64Data);
                success(base64Data);
            },error);
        }

    }
}

function toDataUrl(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        };
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.send();
}


