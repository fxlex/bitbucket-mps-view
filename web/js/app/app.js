var repoPath = getParameterByName("repo_path");
var workID  = -1;
var zipFile;
var nodeInfo = [];
var modulePaths = [];
var mpsProjectName;
var searchList;

var standardAspects = ["structure","editor","actions","constraints","behavior","typesystem","refactorings",
    "scripts","intentions","findUsages","plugin","dataFlow","test","textGen","migration","documentation","accessories"];

function getParameterByName(name) {
    var match = new RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function readNodeInfoFromZip() {
    return $.Deferred(function() {
        var self = this;
        readFileContent("svg.zip",function(data) {
            var new_zip = new JSZip();
            new_zip.loadAsync(window.atob(data)).then(function(zip) {
                zipFile = zip;
                $.each( zip.files, function( key, value ) {
                    var info = new NodeInfo().init(value.name);
                    nodeInfo.push(info);
                });
                self.resolve();
            });
        },function(e) {
            showError("Cannot find svg.zip.","Make sure that you are using the MPS plugin to export the zip file into " +
                "the root folder of the project.");
            self.reject();
        });
    });
}

function readMPSNameFile() {
    return $.Deferred(function() {
        var self = this;
        readFileContent(".mps/.name",function(data) {
            mpsProjectName = data;
            self.resolve();
        }, function(e) {
            showError("File .mps/.name not found.","Make sure that you commit the hidden .mps folder.");
            self.reject();
        });
    });
}

function readMPSModuleFile() {
    return $.Deferred(function() {
        var self = this;
        readFileContent(".mps/modules.xml",function(data) {
            var modulesXML = $.parseXML(data);
            var projectModules = $(modulesXML).find("projectModules");
            projectModules.children().each(function(){
                var path = $(this).attr("path");
                path = path.substr(path.indexOf("/")+ 1); // we don't care about the $PROJECT_DIR$ part
                modulePaths.push(path);
            });
            self.resolve();
        }, function(e) {
            showError("File .mps/modules.xml not found.","Make sure that you commit the hidden .mps folder.");
            self.reject();
        });
    });
}

function routeLanguages() {
    routeModuleOverView("language","#languages","namespace",isLanguage);
}

function isLanguage(value) {
    return value.endsWith(".mpl");
}

function routeSolutions() {
    routeModuleOverView("solution","#solutions","name",isSolution);
}

function isSolution(value) {
    return value.endsWith(".msd");
}

function routeModuleOverView(moduleName,moduleLink,attrName,condition) {
    clearListAndSVG();
    resetNavigationLevel(mpsProjectName);
    addNavigationLevel(moduleName + "s",moduleLink);

    $(modulePaths.filter(condition)).each(function(index,value) {
        readFileContent(value,function(data) {
            var xml = $.parseXML(data);
            var name = $(xml).children().first().attr(attrName);
            var info = new NodeInfo();
            info.module = name;
            info.type = moduleName;

            var dependencies = [];

            var dependenciesXML = $(xml).find("dependencies");
            dependenciesXML.children().each(function(){
                var name = $(this).text();
                name = name.substring(name.indexOf("(")+1,name.indexOf(")")); // we don't care about the unique id
                dependencies.push({name: name});
            });
            var templateData = {dependency: dependencies, index: index };
            var additional =
                $.templates('<div id="expander-module-{{:index}}" class="aui-expander-content">' +
                '   <table class="aui">' +
                '        <thead>' +
                '            <tr>' +
                '                <th id="basic-username">Dependencies</th>' +
                '            </tr>' +
                '        </thead>' +
                '        <tbody>' +
                '            {{for dependency}}' +
                '            <tr>' +
                '                <td headers="basic-username">{{:name}}</td>' +
                '            </tr>' +
                '            {{/for}}' +
                '        </tbody>' +
                '    </table>' +
                '</div>' +
                '<a id="replace-text-trigger" data-replace-text="Less info" class="aui-expander-trigger" aria-controls="expander-module-{{:index}}">More info</a>');
            var title = info.module;
            if(info.type == "language" || info.type == "solution") {
                var models = nodeInfo.filter(function(value) { return value.module === info.module; })
                    .map(function(value) { return value.model});
                var numModels = models.filter(function(elem, pos) {
                    return models.indexOf(elem) == pos;
                }).length;
                title  += " (" + numModels + " " + pluralize("model",numModels) + ")";
            }
            getIcon(localPath(info.type + ".png"),function(iconPath) {
                addItem(title,moduleName,info.linkForModule(),iconPath/*,additional.render(templateData)*/);
            },showDefaultError);

        },function(e) {
            console.log(e.message);
        });
    });

}

function routeModule(moduleType, moduleName) {
    workID = 3;
    var urlInfo = new NodeInfo();
    urlInfo.type = moduleType;
    urlInfo.module = moduleName;
    clearListAndSVG();
    resetNavigationLevel(mpsProjectName);
    urlInfo.buildNavigation(params.MODULE);
    var foundModels = {};

    var nodeInfoSortedByModel = nodeInfo.slice().sort(function(a,b) {
        var indexA = standardAspects.indexOf(a.model);
        if(indexA == -1)
            indexA = standardAspects.length;
        var indexB = standardAspects.indexOf(b.model);
        if(indexB == -1)
            indexB = standardAspects.length;
        return (indexA < indexB) ? -1 : (indexA > indexB) ? 1 : 0;
    });

    $.each(nodeInfoSortedByModel, function(index, info ) {
        if(!info.equals(urlInfo) ||  foundModels[info.model] !== undefined)
            return true;

        foundModels[info.model] = true;
        var iconPathToFetch = info.type === "solution" ? localPath("model.png") : info.conceptNode + ".png";
        getIcon(iconPathToFetch,function(iconPath) {
            var title = info.model;
            if(info.type === "generator")
                title = "generator/" + displayName;

            var nodes = nodeInfo.filter(function(value) { return value.module === info.module && value.model == info.model; })
                .map(function(value) { return value.node});
            var numNodes = nodes.filter(function(elem, pos) {
                return nodes.indexOf(elem) == pos;
            }).length;
            title  += " (" + numNodes + " " + pluralize("node",numNodes) + ")";

            addItem(title, info.conceptNode, info.linkForModel(), iconPath);
        },showDefaultError);
    });
}

function routeModel(moduleType, moduleName,modelName) {
    workID = 4;
    var urlInfo = new NodeInfo();
    urlInfo.type = moduleType;
    urlInfo.module = moduleName;
    urlInfo.model = modelName;
    clearListAndSVG();
    resetNavigationLevel(mpsProjectName);
    urlInfo.buildNavigation(params.MODEL);

    $.each(nodeInfo, function(index, info ) {
        if(!info.equals(urlInfo))
            return;

        getIcon(info.conceptNode + ".png",function(iconPath) {
            if(workID != 4) return;

            addItem(info.node, info.conceptNode, info.linkForNode(), iconPath);
        },showDefaultError);
    });
}

function routeNode(moduleType,moduleName,modelName,nodeID,node) {
    workID = 5;
    var urlInfo = new NodeInfo();
    urlInfo.type = moduleType;
    urlInfo.module = moduleName;
    urlInfo.model = modelName;
    urlInfo.nodeID = nodeID;
    urlInfo.node = node;
    clearListAndSVG();
    resetNavigationLevel(mpsProjectName);
    urlInfo.buildNavigation(params.NODE);

    $.each(nodeInfo, function(index, info ) {
        if(!info.equals(urlInfo))
            return;
        zipFile.file(info.nodePath).async("string").then(function (data) {
            $('#svg').html(data);
            return false;
        });
    });
}

function routeHome() {
    clearListAndSVG();
    resetNavigationLevel(mpsProjectName);
    getIcon(localPath("language.png"),function(iconPath) {
        var title = "languages (" + modulePaths.filter(isLanguage).length + ")";
        addItem(title,"Module","#languages",iconPath);
    },showDefaultError);

    getIcon(localPath("solution.png"),function(iconPath) {
        var title = "solutions (" + modulePaths.filter(isSolution).length + ")";
        addItem(title,"Module","#solutions",iconPath);
    },showDefaultError);

}

function routeViewModuleFile(path) {
    var modulePath = path.split("&")[0];
    console.log(modulePath);
    var info = new NodeInfo();
    info.type = getModuleKind(modulePath);
    readFileContent(modulePath,function(data) {
        var xml = $.parseXML(data);
        var attrName =  info.type === "solution"? "name": "namespace";
        info.module = $(xml).children().first().attr(attrName);
        routie(info.linkForModule());
    },function(e) {
        console.log(e.message);
    });
}

function setupRoutes() {
    routie({
        'solutions': routeSolutions,
        'languages': routeLanguages,
        'type/:moduleType/module/:moduleName': routeModule,
        'view/*': routeViewModuleFile,
        'type/:moduleType/module/:moduleName/model/:modelName': routeModel,
        'type/:moduleType/module/:moduleName/model/:modelName/nodeID/:nodeID/nodeName/:nodeName': routeNode,
        '': routeHome
    });
}

$.when(readNodeInfoFromZip(),readMPSNameFile(),readMPSModuleFile())
    .done(setupRoutes)
    .fail(function() {
        $("body").prepend(
            '<div class="aui-message info">' +
                '<p class="title">' +
                    '<span class="aui-icon icon-error"></span>'  +
                    '<strong>Nothing to display.</strong>'  +
                '</p>'  +
            '</div>')
    });


$(document).ready(function() {
    AJS.$('.conceptNodeToolTip').tooltip();
});