# Bitbucket MPS add-on

This Bitbucket allows to view projects build with [JetBrains MPS]

## Installation

1. Browse to **Manage account** > **Manage add-ons**.
2. Click **Install add-on from URL**.
3. Enter `https://bitbucket-mps-view.aerobatic.io/web/connect.json` in the **Add-on URL** field and click **Install**.
4. You should now see a **Jetbrains MPS View** link in the sidebar.

## Development

To develop locally:

1. Clone this repository locally.
2. Start up a simple python webserver with `python -m SimpleHTTPServer`.
3. Install [ngrok] and proxy the web server to the public internet with `ngrok 8000`.
4. Browse to **Manage account** > **OAuth** and create a new OAuth consumer. **Important: you must set the Callback URL to the `https://` URL provided by ngrok**
5. Change the **baseUrl** and **oauthConsumer clientId** in `connect.json` to the `https://` URL (including a trailing slash) and OAuth consumer key, respectively.
6. Browse to **Manage account** > **Manage add-ons**.
7. Click **Install add-on from URL**.
8. Enter `https://$NGROK_BASE_URL/web/connect.json` in the **Add-on URL** field and click **Install** (replace `$NGROK_BASE_URL` with the actual `https://` base URL provided by ngrok)
9. You should now see a **File types** link in the left-hand **Navigation** menu on all Bitbucket repositories.

[Jetbrains MPS]: https://www.jetbrains.com/mps/
[ngrok]: https://ngrok.com/